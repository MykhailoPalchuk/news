//
//  ArticleDetailTextCell.swift
//  News
//
//  Created by Mykhailo Palchuk on 2/9/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

class ArticleDetailTextCell: UITableViewCell {
    
    // MARK: - Outlets
    
    @IBOutlet weak var label: UILabel!
    
    // MARK: - Properties
    
    static let reuseIdentifier = "ArticleDetailTextCell"
    
    // MARK: - Methods
    
    func setup(text: String, font: UIFont) {
        label.text = text
        label.font = font
    }
}
