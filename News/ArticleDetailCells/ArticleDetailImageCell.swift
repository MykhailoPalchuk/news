//
//  ArticleDetailImageCell.swift
//  News
//
//  Created by Mykhailo Palchuk on 2/9/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

class ArticleDetailImageCell: UITableViewCell {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var customImageView: UIImageView!
    
    // MARK: - Properties
    
    static let reuseIdentifier = "ArticleDetailImageCell"
    
    // MARK: - Methods
    
    func setImage(_ image: UIImage?) {
        customImageView.image = image ?? UIImage(named: "DefaultImage")
    }
}
