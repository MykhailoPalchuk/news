//
//  ArticleDetailLinkCell.swift
//  News
//
//  Created by Mykhailo Palchuk on 2/9/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

class ArticleDetailLinkCell: UITableViewCell {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var textView: UITextView!
    
    // MARK: - Properties
    
    static let reuseIdentifier = "ArticleDetailLinkCell"
    
    private var openUrlAction: ((URL) -> Void)?
    
    // MARK: - Methods
    
    func setup(text: String, link: URL, openUrlAction: ((URL) -> Void)?) {
        let attributedString = NSMutableAttributedString(string: text)
        let attributesRange = NSRange(location: 0, length: text.count)
        attributedString.addAttribute(.link, value: link, range: attributesRange)
        
        let style = NSMutableParagraphStyle()
        style.alignment = .center
        attributedString.addAttribute(.paragraphStyle, value: style, range: attributesRange)
        
        let font = UIFont.systemFont(ofSize: 17)
        attributedString.addAttribute(.font, value: font, range: attributesRange)
        
        textView.attributedText = attributedString
        self.openUrlAction = openUrlAction
        textView.delegate = self
    }
}

// MARK: - UITextViewDelegate

extension ArticleDetailLinkCell: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        openUrlAction?(URL)
        return false
    }
}
