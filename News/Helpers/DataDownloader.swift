//
//  DataDownloader.swift
//  News
//
//  Created by Mykhailo Palchuk on 2/7/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct UnsecureUrlError: Error {
    var localizedDescription: String
    
    init() {
        localizedDescription = "http URLs are not allowed"
    }
}

class DataDownloader {
    func download(url: URL) -> Observable<Data> {
        return Observable<Data>.create { observer in
            if !url.absoluteString.contains("https") {
                observer.onError(UnsecureUrlError())
                observer.onCompleted()
                return Disposables.create()
            }
            let task = URLSession.shared.dataTask(with: URLRequest(url: url)) { data, response, error in
                if let error = error {
                    observer.onError(error)
                } else if let data = data {
                    observer.onNext(data)
                }
                observer.onCompleted()
            }
            task.resume()
            
            return Disposables.create {
                task.cancel()
            }
        }
    }
}
