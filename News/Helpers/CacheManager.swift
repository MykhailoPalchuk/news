//
//  CacheManager.swift
//  News
//
//  Created by Mykhailo Palchuk on 2/10/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import Foundation

class CacheManager {
    
    // MARK: - Constants
    
    private let cacheFileName = "cache"
    
    // MARK: - Properties
    
    private var fileUrl: URL? {
        return FileManager.default
            .urls(for: .documentDirectory, in: .userDomainMask)
            .first?
            .appendingPathComponent(cacheFileName)
    }
    
    // MARK: - Methods
    
    func getCache() -> [Article]? {
        if let fileUrl = fileUrl {
            do {
                let data = try Data(contentsOf: fileUrl)
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .iso8601
                let articles = try decoder.decode([Article].self, from: data)
                return articles
            } catch {
                print("Error reading cache: \(error)")
            }
        }
        return nil
    }
    
    func saveCache(_ articles: [Article]) {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .iso8601
        do {
            let json = try encoder.encode(articles)
            if let fileUrl = fileUrl {
                try json.write(to: fileUrl)
            }
        } catch {
            print("Error saving cache: \(error)")
        }
    }
    
    func deleteCache() {
        if let fileUrl = fileUrl {
            try? Data().write(to: fileUrl)
        }
    }
}
