//
//  Date+Extensions.swift
//  News
//
//  Created by Mykhailo Palchuk on 2/9/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import Foundation

extension Date {
    var string: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d, yyyy, hh:mm a"
        return formatter.string(from: self)
    }
}
