//
//  NewsFeedController.swift
//  News
//
//  Created by Mykhailo Palchuk on 2/5/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol NewsFeedControllerDelegate {
    func loadData()
    func reloadData()
    func loadNextPage()
    func articleSelected(_ article: Article)
}

class NewsFeedController: UITableViewController {
    
    // MARK: - Properties
    
    var delegate: NewsFeedControllerDelegate?
    
    private var dataSource = BehaviorRelay(value: [Article]())
    
    private let downloader = DataDownloader()
    private let disposeBag = DisposeBag()
    
    // MARK: - Overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.refreshControl = refreshControl
        
        tableView.register(UINib(nibName: "ArticleCell", bundle: nil), forCellReuseIdentifier: ArticleCell.reuseIdentifier)
        tableView.dataSource = nil
        tableView.delegate = nil
        dataSource.bind(to: self.tableView.rx.items(cellIdentifier: ArticleCell.reuseIdentifier, cellType: ArticleCell.self)) { index, model, cell in
            cell.setup(title: model.title, description: model.description)
            if let imageData = model.image {
                cell.setImage(image: UIImage(data: imageData))
            }
        }.disposed(by: disposeBag)
        
        tableView.rx.modelSelected(Article.self)
            .subscribe(onNext: { self.delegate?.articleSelected($0) })
            .disposed(by: disposeBag)
        
        tableView.rx.willDisplayCell
            .subscribe(onNext: { cell, indexPath in
                if indexPath.row == self.dataSource.value.count - 1 {
                    // Time to load next page
                    self.delegate?.loadNextPage()
            }
        }).disposed(by: disposeBag)
        
        delegate?.loadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.title = "News"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !Network.reachability.isReachable {
            showOfflineStatus()
        }
    }
    
    // MARK: - Methods
    
    func refreshContent(_ content: [Article]) {
        DispatchQueue.main.async {
            self.dataSource.accept(content)
            self.refreshControl?.endRefreshing()
        }
    }
    
    func showOfflineStatus() {
        refreshControl?.endRefreshing()
        navigationController?.navigationBar.backgroundColor = .red
        navigationController?.navigationBar.barTintColor = .red
        navigationItem.title = "Looks like you're offline :["
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.navigationController?.navigationBar.backgroundColor = nil
            self.navigationController?.navigationBar.barTintColor = nil
            self.navigationItem.title = "News"
        }
    }
    
    // MARK: - Private
    
    @objc
    private func refreshData() {
        delegate?.reloadData()
    }
}

