//
//  NewsFeedCoordinator.swift
//  News
//
//  Created by Mykhailo Palchuk on 2/9/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SafariServices

typealias ArticleImageLoadingTask = Observable<(articleId: String, loader: Observable<Data>?)>

class NewsFeedCoordinator: NSObject, Coordinator {
    
    // MARK: - Constants
    
    let country = "us"
    let defaultPageSize = 20
    
    // MARK: - Properties
    
    private let presenter: UINavigationController
    private let feedViewController: NewsFeedController
    
    private let helper: APIHelper
    private let downloader: DataDownloader
    private let disposeBag: DisposeBag
    
    private var pageLoader: Disposable?
    private var imageLoaderTasks: [Disposable]
    private var articles: [Article]
    private var totalArticlesCount: Int
    private var currentPage: Int
    
    private var cacheManager: CacheManager
    
    // MARK: - Init
    
    init(presenter: UINavigationController) {
        self.presenter = presenter
        self.feedViewController = NewsFeedController(style: UITableView.Style.plain)
        self.helper = APIHelper()
        self.downloader = DataDownloader()
        self.disposeBag = DisposeBag()
        self.articles = []
        self.totalArticlesCount = 0
        self.currentPage = 0
        self.imageLoaderTasks = []
        self.cacheManager = CacheManager()
        super.init()
        
        feedViewController.delegate = self
    }
    
    // MARK: - Methods
    
    func start() {
        presenter.pushViewController(feedViewController, animated: true)
    }
    
    // MARK: - Private
    
    private func fetchPage() {
        if pageLoader != nil {
            // Dispose previous loading task
            pageLoader?.dispose()
            pageLoader = nil
        }
        
        if totalArticlesCount == 0 || articles.count < totalArticlesCount {
            print("Loading page: \(currentPage + 1)")
            let request = TopHeadlinesRequest(country: country, pageSize: defaultPageSize, page: currentPage + 1)
            let requestTask: Observable<APIResponse> = helper.send(apiRequest: request)
            pageLoader = requestTask.asObservable()
                .subscribe(onNext: { (response) in
                    self.articles.append(contentsOf: response.articles)
                    self.cacheManager.saveCache(self.articles)
                    self.feedViewController.refreshContent(self.articles)
                    self.totalArticlesCount = response.totalResults
                    self.fetchImages()
                    self.currentPage += 1
                    print("Articles loaded. Count: \(self.articles.count) of \(self.totalArticlesCount)")
                }, onError: { (error) in
                    print("Error loading data: \(error)")
                    let usefulError = error as NSError
                    if usefulError.code == -1009 {
                        // We're offline
                        DispatchQueue.main.async {
                            self.feedViewController.showOfflineStatus()
                        }
                    }
                }, onCompleted: {
                    self.pageLoader?.dispose()
                    self.pageLoader = nil
                })
        }
    }
    
    private func fetchImages() {
        imageLoaderTasks.forEach { $0.dispose() }
        imageLoaderTasks = []
        
        Observable.from(articles)
            .flatMap { (article) -> ArticleImageLoadingTask in
                if let urlToImage = article.urlToImage, let url = URL(string: urlToImage) {
                    return Observable.just((article.id, self.downloader.download(url: url)))
                }
                return Observable.just((article.id, nil))
            }.subscribe(onNext: { (task) in
                if let dataLoader = task.loader {
                    let imageLoaderTask = dataLoader.subscribe(onNext: { (data) in
                        if let index = self.articles.firstIndex(where: { $0.id == task.articleId }) {
                            self.articles[index].image = data
                        }
                        self.feedViewController.refreshContent(self.articles)
                        self.cacheManager.saveCache(self.articles)
                    })
                    self.imageLoaderTasks.append(imageLoaderTask)
                }
            })
        .disposed(by: disposeBag)
    }
}

// MARK: - NewsFeedControllerDelegate

extension NewsFeedCoordinator: NewsFeedControllerDelegate {
    func loadData() {
        articles = cacheManager.getCache() ?? []
        if articles.count == 0 {
            fetchPage()
        } else {
            feedViewController.refreshContent(articles)
        }
    }
    
    func reloadData() {
        guard Network.reachability.isReachable else {
            feedViewController.showOfflineStatus()
            return
        }
        
        articles = []
        cacheManager.deleteCache()
        totalArticlesCount = 0
        currentPage = 0
        fetchPage()
    }
    
    func loadNextPage() {
        fetchPage()
    }
    
    func articleSelected(_ article: Article) {
        let articleController = ArticleDetailViewController(style: .plain)
        articleController.article = article
        articleController.delegate = self
        presenter.pushViewController(articleController, animated: true)
    }
}

// MARK: - ArticleDetailViewControllerDelegate

extension NewsFeedCoordinator: ArticleDetailViewControllerDelegate {
    func open(url: URL, inside target: UIViewController) {
        let safari = SFSafariViewController(url: url)
        safari.delegate = self
        target.present(safari, animated: true)
    }
}

// MARK: - SFSafariViewControllerDelegate

extension NewsFeedCoordinator: SFSafariViewControllerDelegate {
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        presenter.popViewController(animated: true)
    }
}
