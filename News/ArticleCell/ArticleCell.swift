//
//  ArticleCell.swift
//  News
//
//  Created by Mykhailo Palchuk on 2/7/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

class ArticleCell: UITableViewCell {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    // MARK: - Properties
    
    static let reuseIdentifier = "ArticleCell"
    
    // MARK: - Methods
    
    func setup(title: String, description: String?) {
        titleLabel?.text = title
        descriptionLabel?.text = description
        setImage(image: nil)
    }
    
    func setImage(image: UIImage?) {
        articleImageView?.image = image ?? UIImage(named: "DefaultImage")
    }
}
