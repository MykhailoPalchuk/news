//
//  ArticleDetailViewController.swift
//  News
//
//  Created by Mykhailo Palchuk on 2/10/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit
import RxDataSources
import RxSwift
import RxCocoa

enum SectionModel {
    case theOneAndOnlySection(items: [TableItemModel])
}

extension SectionModel: SectionModelType {
    typealias Item = TableItemModel
    
    var items: [TableItemModel] {
        switch self {
        case .theOneAndOnlySection(items: let items):
            return items
        }
    }
    
    init(original: SectionModel, items: [TableItemModel]) {
        self = .theOneAndOnlySection(items: items)
    }
}

enum TableItemModel {
    case text(text: String, font: UIFont)
    case image(image: UIImage?)
    case link(text: String, link: URL)
}

protocol ArticleDetailViewControllerDelegate {
    func open(url: URL, inside: UIViewController)
}

class ArticleDetailViewController: UITableViewController {
    
    // MARK: - Properties
    
    var article: Article?
    var delegate: ArticleDetailViewControllerDelegate?
    
    private var disposeBag = DisposeBag()
    
    // MARK: - Overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "ArticleDetailTextCell", bundle: nil),
                           forCellReuseIdentifier: ArticleDetailTextCell.reuseIdentifier)
        tableView.register(UINib(nibName: "ArticleDetailImageCell", bundle: nil),
                           forCellReuseIdentifier: ArticleDetailImageCell.reuseIdentifier)
        tableView.register(UINib(nibName: "ArticleDetailLinkCell", bundle: nil),
                           forCellReuseIdentifier: ArticleDetailLinkCell.reuseIdentifier)
        
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.title = article?.source.name
        
        guard let article = self.article else {
            // Nothing to show
            return
        }
        
        var items: [TableItemModel] = []
        
        // Title
        items.append(.text(text: article.title, font: UIFont.systemFont(ofSize: 17, weight: .semibold)))
        
        // Image
        if let imageData = article.image {
            items.append(.image(image: UIImage(data: imageData)))
        }
        
        // Time
        items.append(.text(text: article.publishedAt.string, font: UIFont.systemFont(ofSize: 14, weight: .light)))
        
        // Description
        items.append(.text(text: article.description ?? "No description", font: UIFont.systemFont(ofSize: 17, weight: .medium)))
        
        // Content
        items.append(.text(text: article.content ?? "No preview available", font: UIFont.systemFont(ofSize: 15)))
        
        // Link
        if let url = URL(string: article.url) {
            items.append(.link(text: "Show full article", link: url))
        }
        
        let sections = [SectionModel.theOneAndOnlySection(items: items)]
        tableView.dataSource = nil
        tableView.delegate = nil
        let dataSource = instantiateDataSource()
        Observable.just(sections)
        .bind(to: tableView.rx.items(dataSource: dataSource))
        .disposed(by: disposeBag)
    }
    
    // MARK: - Methods
    
    private func instantiateDataSource() -> RxTableViewSectionedReloadDataSource<SectionModel> {
        return RxTableViewSectionedReloadDataSource<SectionModel>(configureCell: { (section, tableView, indexPath, item) -> UITableViewCell in
            switch item {
            case .text(text: let text, font: let font):
                let cell = tableView.dequeueReusableCell(withIdentifier: ArticleDetailTextCell.reuseIdentifier) as! ArticleDetailTextCell
                cell.setup(text: text, font: font)
                return cell
            case .image(image: let image):
                let cell = tableView.dequeueReusableCell(withIdentifier: ArticleDetailImageCell.reuseIdentifier) as! ArticleDetailImageCell
                cell.setImage(image)
                return cell
            case .link(text: let text, link: let url):
                let cell = tableView.dequeueReusableCell(withIdentifier: ArticleDetailLinkCell.reuseIdentifier) as! ArticleDetailLinkCell
                cell.setup(text: text, link: url, openUrlAction: { self.delegate?.open(url: $0, inside: self) })
                return cell
            }
        })
    }
}
