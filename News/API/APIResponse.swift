//
//  APIResponse.swift
//  News
//
//  Created by Mykhailo Palchuk on 2/6/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

struct APIResponse: Codable {
    let status: String
    let totalResults: Int
    let articles: [Article]
}

class Article: Codable {
    let id: String = UUID().uuidString
    let source: Source
    let author: String?
    let title: String
    let description: String?
    let url: String
    let urlToImage: String?
    var image: Data?
    let publishedAt: Date
    let content: String?
}

class Source: Codable {
    let name: String = ""
}
