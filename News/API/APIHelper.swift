//
//  APIHelper.swift
//  News
//
//  Created by Mykhailo Palchuk on 2/6/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

let apiURL = URL(string: "https://newsapi.org/v2")!
let apiKey = "48489189fb524eb48025884e1f6329ac"

protocol APIRequest {
    var path: String { get }
    var parameters: [String : String] { get }
}

extension APIRequest {
    func request(with baseURL: URL) -> URLRequest {
        guard var components = URLComponents(url: baseURL.appendingPathComponent(path),
                                             resolvingAgainstBaseURL: false) else {
            fatalError("Unable to create URL components")
        }
        
        components.queryItems = parameters.map {
            URLQueryItem(name: String($0), value: String($1))
        }
        
        guard let url = components.url else {
            fatalError("Could not get url")
        }
        
        return URLRequest(url: url)
    }
}

class APIHelper {
    func send<T: Codable>(apiRequest: APIRequest) -> Observable<T> {
        return Observable<T>.create { observer in
            let request = apiRequest.request(with: apiURL)
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                if let error = error {
                    observer.onError(error)
                    observer.onCompleted()
                } else {
                    do {
                        let coder = JSONDecoder()
                        coder.dateDecodingStrategy = .iso8601
                        let model: T = try coder.decode(T.self, from: data ?? Data())
                        observer.onNext(model)
                    } catch let error {
                        observer.onError(error)
                    }
                    observer.onCompleted()
                }
            }
            task.resume()
            
            
            return Disposables.create {
                task.cancel()
            }
        }
    }
}
