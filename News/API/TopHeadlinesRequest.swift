//
//  TopHeadlinesRequest.swift
//  News
//
//  Created by Mykhailo Palchuk on 2/7/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import Foundation

class TopHeadlinesRequest: APIRequest {
    var path: String
    var parameters: [String : String]
    
    init(country: String, pageSize: Int, page: Int = 1) {
        path = "top-headlines"
        parameters = ["country": country,
                      "pageSize": String(pageSize),
                      "page": String(page),
                      "apiKey": apiKey]
    }
}
