//
//  AppCoordinator.swift
//  News
//
//  Created by Mykhailo Palchuk on 2/9/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

class AppCoordinator: Coordinator {
    
    private let window: UIWindow
    private let rootViewController: UINavigationController
    private let feedCoordinator: NewsFeedCoordinator
    
    init(window: UIWindow) {
        self.window = window
        self.rootViewController = UINavigationController()
        self.feedCoordinator = NewsFeedCoordinator(presenter: rootViewController)
    }
    
    func start() {
        window.rootViewController = rootViewController
        feedCoordinator.start()
        window.makeKeyAndVisible()
    }
}
