# News

News application fetches top headlines from [News API](https://newsapi.org) and displays them as feed. 
Tap on the article to see more details and get link to the source.
Latest articles are saved to local file as json so you can read them offline :]

This project is build with RxSwift.
Icon made by [Freepic](https://www.flaticon.com/authors/freepik) from [FlatIcon](www.flaticon.com) 

